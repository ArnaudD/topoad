# TopoAD

Programmes de calculs topométriques et de géodésie pour TI-89.

![Screenshot](screenshots/screenshots.jpg?raw=true)

## Installation
Transférez simplement les programmes dans votre calculatrice. Une fois transférés, il n'y a pas d'étape d'installation supplémentaire.

## Utilisation
Le lanceur principal est le programme kbdprgm2(). Il peut être lancé directement via le racourci *diamant+2*.

## Liste des programmes
- angles:	Conversion d'angles
- antenne:	Cheminement polygonal en antenne
- arcapabl:	Résolution d'un arc capable
- arcgcos:	arccos en grades (fonction)
- arcgsin:	arcsin en grades (fonction)
- arctan:	arctan en grades (fonction)
- artdb:	Raccordement ARTDB
- distance:	Distance entre deux points
- distptd:	Calcul de la distance point-droite
- editpts:	Ajout de point
- encadre:	Cheminement polygonal encadré
- excentre:	Excentrements de station
- fautes:	Détection de fautes dans un cheminement polygonal
- gcos:		cos en grades (fonction)
- gdxy:		Conversion gisement distance en x y
- gisement:	Gisement entre deux points
- go:		Go de station
- gsin:		sin en grades (fonction)
- gtan:		tan en grades (fonction)
- intercc:	Intersection cercle-cercle
- intercd:	Intersection droite-cercle
- interdd:	Intersection droite-droite
- lambert:	Réduction de distances à la projection lambert
- lpxy93:	Conversion de coordonnées géographiques en lambert 93
- lpxyz:	Conversion de coordonnées géographiques en lambert zone
- milieu:	Coordonnées du milieu entre deux points
- modifpts:	Modification des coordonnées d'un point
- nivelmnt:	Nivellement
- profils:	Calcul de profils
- rayonmnt:	Rayonnement
- rect:		Surface par coordonnées
- relevmnt:	Relèvement sur trois points
- segcap:	Segment capable
- segdist:	Segment distance
- supprpts:	Suppression d'un point
- temps:	Conversion horaire
- triangle:	Résolution des triangles plans
- trispher:	Résolution des triangles sphériques
- visee:	Visée methode de Hatt
- xygd:		Conversion de coordonnées en gisement distance
- xylp93:	Conversion de coordonnées lambert 93 en coordonnées géographiques
- xylpz:	Conversion de coordonnées lambert zone en coordonnées géographiques

## Status
Les programmes ont été développés pendant mes études (2003-2008) et ne sont plus maintenus. Ils sont partagés à titre historique !

Si vous avez des questions cependant, vous pouvez me contacter.

Arnaud

